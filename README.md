# DEPRECATED: July, 2024 - location moved to https://gitlab.com/Linaro/onelab/documentation/customer/onelab/onelab-user-guide 


# Linaro Remote Labs

This repo contains the source for Linaro Remote Labs documentation.
When updated, it will render a new readthedocs web-based users manual located
here(https://linaro-remote-labs.readthedocs.io/en/latest/)

Contributions are welcome!  

The recommended process for contributing to this document are as follows:
1) Submit an issue to the repo for any problems found.
2) Make more substantial changes to the documentation such as new/appended
sections. It's suggested to start with an issue prior to making substantial
changes to attain alignment with the document maintainers to avoid wasted
effort.

## Heading Format
Heading formats shall follow this guideline (Python Dev Guide convention)
```
HEADING 1
##############

Heading 2
*********

Heading 3
=========

Subsections
-----------

Subsubsections
^^^^^^^^^^^^^^

Level 6 if needed
"""""""""""""""""
```

## Repository Merge Request submission process:

- Fork the repository to your own Gitlab account. [Set up an
  account](https://gitlab.com/users/sign_up) if you don't already have one. 
- On your local development machine:
  - Set up the Sphinx development environment. This includes pip install (or
    equivalent) of the python libraries found in the *docs/requirements.txt*
file.  
- Using git from your development machine working directory, clone the forked
  repository from personal gitlab account
- Perform a build locally to confirm it's building - resolve issues if not.

```
make clean
make html
```

- Once building locally make your edits/adds to the local repo
- Keep source files to 80 characters per line. Example using
  vi editor below:

```
:set  textwidth=80
gg 	// go to top of file
gqG // formats all lines to end of file to 80
:wq

To update a single line:

shift-j     // to join line that want to format
gq <enter>  // adjusts to the 80 character max
```

- Use squash to reduce to the minimum commits. Example [here](https://medium.com/@slamflipstrom/a-beginners-guide-to-squashing-commits-with-git-rebase-8185cf6e62ec)

- It's anticipated that his repository will be getting updated often, thus it's
  a good idea to [rebase and resolve any
conflicts](https://docs.gitlab.com/ee/topics/git/git_rebase.html) prior to
sending a Merge Request. 
- Run a spell checker on your updates. For example, using the vi editor `:set spell
  spelllang=us_en`. 
- *Always* run the following locally to assure it's building as the last step
 prior to committing and updating your work in preparation for a MR.  From docs
directory in the repo:

```
make clean
make html
```

- Now ready to update your Gitlab Repo. From command line: 

```
git status 				// see files changed, added/deleted
git add <filename of any new files>
git add .               // adds all at once
git add -u .            // to include cases when files have been deleted
git commit -m "New message here"	// Commits changes to your repo
git push origin main 			// Assumes you've already sent up remote repo using git-remote
```

- From your forked and updated Gitlab repo, perform the [Merge Request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html).

And thanks for your contributions!

## Local Desktop Development Setup
This section shares how to set up and run a local instance of the docs on a Mac.
Linux is similar.

Install [Sphinx Autobuild](https://sphinx-extensions.readthedocs.io/en/latest/sphinx-autobuild.html)

```
pip install sphinx-autobuild
```

Go to you docs working directory in the active repo 

```
cd ~/dev/linaro-remote-labs/docs/
```

Run auto-build to bring up local active instance

```
sphinx-autobuild . _build/html
```

When it runs, it will provide the local instance that your docs will be served
up on. 

```
Serving on http://127.0.0.1:8000
```

Open a local browser and copy-paste the url.  Your docs will now be running
locally.  When you make changes in your working directory, they are built and
rendered into the browser real-time.

## Make a pdf
If you need to have someone review progress that isn't set up for active
development, you can create a pdf instance "snapshot" of your docs. To do so do
the following:

On a Mac, download and install LaTex.  
Reboot your machine to update paths.
From terminal, check for install

```
which latex
/Library/TeX/texbin/latex
```

Ready now to build a pdf.  From you docs active repository working directory

```
make latexpdf
cd _build/latex/
ls *pdf
```

Note: To remove blank pages in the printout, edit the ./docs/conf.py, navigate
to the letex_elements class and add the following classoptions parameter:

```
latex_elements = {
    'classoptions': ',openany',
}
```

Can now use finder to open the pdf or to send it out for review


<end>
