..
 # Copyright (c) 2022, Arm Limited.
 # Copyright (c) 2023, Linaro Ltd.
 #
 # SPDX-License-Identifier: MIT

Overview
######## 

Introduction
************

For the last 10+ years Linaro has spearheaded the upstreaming and stability of
Arm processors in the Open Source Ecosystem.  Linaro’s Remote Labs take that
expertise to the next level by providing flexibility in the spin up, maintenance, and device sharing of embedded validations labs over a cloud-based infrastructure. 

Leveraging Linaro's Remote Labs technology solves another need within the Arm
ecosystem, which is the creation of standardized compliance validation labs.
Linaro is starting out by enabling with an IoT lab instance that is
focused on SystemReady IR Parsec, and PSA compliance. It's anticipated that
other ONELab instances focused on additional ecosystems will follow, but initial
focus is to assure ONELab IoT is working smoothly prior to considering other
variants.  These labs will be available for companies to validate and maintain
their platforms against industry standards by hosting their platforms in any of
the applicable lab instances. Leveraging this infrastructure to efficiently
provide assurance of Compliance, Security and Interoperability at Scale provides
multiple benefits including customer confidence and reduced internal lab
infrastructure costs. For much more detailed information on the specific ONELab
instances available for participation in please see the :ref:`linaro_labs`
section of this document.

In the future, there may also be cases where a customer may want to leverage the
Linaro Remote Labs infrastructure for private, or non-compliance, applications.
This will also be possible through the Linaro Remote Labs infrastructure. This
will be an attractive solution for companies that want to free themselves of the
overhead and limitations of a single physical lab hosting their entire
infrastructure.  The :ref:`hlo` section below provides a brief summary of how
Linaro Remote Labs can provide benefits to an organization.
 
Goals and key properties 
************************

The goals of Linaro Remote Labs includes the following:

* To provide a flexible infrastructure that supports embedded target
  validation and test over the cloud
* To eliminate the traditional burden and limitations of traditional on-premises
  embedded lab infrastructure
* To support sharing of embedded Devices Under Test(DUT) for the use cases when
  platforms are limited
* To provide compliance confidence in the :ref:`linaro_labs` of which a platform
  is stood up in.

.. _hlo:

High-level overview 
*******************

Linaro Remote Labs is based on Linaro's LAVA Managed Services(LMS). LMS is a
powerful cloud-based infrastructure created by Linaro that orchestrates
containerized LAVA servers in the cloud. This ground-breaking technology
provides the benefits of scalability and currency while freeing up an
organization of the worries of maintaining an on-premises Test Automation
Framework(TAF). Coupled with the :ref:`laa_intro`, this this total solution
breaks the norms of a standard embedded lab concept and effectively provides
users with an "embeddeded-labs-as-a-service" solution. A more detailed overview
of LMS can be found in the :ref:`lms_overview` section of this document. To find
more details on the currently supported cloud-based labs Linaro currently
supports, please see the :ref:`linaro_labs` section.


Features overview and use-cases
===============================

Linaro Remote Labs are composed of three primary high-level components. These
include :ref:`lms`, :ref:`managed_servers`, herein referred to as Labs, and the :ref:`laa_intro`.

The LMS is a federated service that deploys the Labs, or containerized LAVA
Server instances, into the cloud as well as managing the LAAs in the LMS domain
to assure they can be shared across available labs while also leveraging
prioritization and mutual exclusion technologies to maximize DUT utilization and
complete validation and test efforts as efficiently as possible.

The Labs can be customized on a case by case basis, providing flexibility in the
types of testing deployments and the number of target devices desired to be used
in any Lab instance.  

And finally, the LAA is a custom hardware solution provided by Linaro. It
consists of a flexible platform to plug a customer DUT into and provides a bridge
between the LMS cloud infrastructure and the Labs to the DUT allowing the
flexibility of leveraging the DUTs for multiple purposes.

This solution provides several benefits including the possibility of a DUT to be
located anywhere globally(as long and internet access is available) and allows
the DUT to be leveraged by multiple Labs. For more information on how this
works, please see the :ref:`lms` section of this document.

The figure below provides one example, wherein LMS has deployed multiple Labs in
the LMS managed cloud.  Each of these labs can have totally different goals/compliance
suites, etc. but through LMS, the Labs can request a specific target and be
provided any one of potentially multiple targets available.

.. image:: ./images/remote_lab_example1a.png

The next example represents a use case where one Lab may be a production testing
instance while another lab may be a staged instance to validate that tests are
executed correctly prior to deploying new tests or updates to a production
instance. Again, it can be seen that each of these two labs can leverage any of
the 4 LAAs available to both LABs. Configuration options in LMS support
prioritization of LAAs. It should be noted that these available LAAs that are
hosting to Devices Under Test can be located remotely from each other. This can
even support the use case where one or more of the DUTs can be accessible by a
customer or developer that is remotely located.

.. image:: ./images/remote_lab_example2.png

The remainder of this document goes into further detail on each of these
technologies as well as leveraging existing :ref:`linaro_labs` deployed by
Linaro and available for participation in.

Terms, Definitions  and Acronyms
********************************

To fill in

Terms/Definitions
=================

* **Architecture Compliance Suites(ACS):** Arms `Architecture Compliance Suites <https://developer.arm.com/Architectures/Architectural%20Compliance%20Suite>`_ are a series of test suites that check the compliance of a system against arm architectural specifications. The Architecture Compliance Suites (ACS) are hosted in GitHub and are open source (Apache v2). Further information on individual suites is provided below.


.. _assessment:

* **Assessment:** TODO

.. _conformance_run:

* **Conformance Run:** A conformance run is the execution of a pre-defined
  ONELab test suite based on a specific version of the following:

  - Platform Firmware
  - Cloud Native Application/Tooling
  - Test Suites (ONELab Lab Type Dependent)

    - SystemReady 
    - Parsec
    - PSA
    - other
  - Operating system

  If the version of any one of the components above are changed, then a new
  conformance run is instantiated
* **Labs:** In the context of this document, Labs is the term used to represent :ref:`managed_servers`

.. _release_stream:

* **Release Stream:** A release stream allow for platforms that need to test
  multiple families of firmware to support different use-cases.  One platform
  may have multiple Release Streams, and a Release Stream may have multiple
  versions of firmware. For example, firmware for a wireless router platform
  may need different release streams to cover different regulatory regimes for
  different regions. 

Acronyms
========
* **ACS:** Arm Compliance Suite
* **DUT:** Device Under Test, the platform being tested in the Linaro Remote Labs.  
* **FOTA:** Firmware Over the Air. Associated for firmware updates.
* **LAA:** LAVA Automation Appliance. See the :ref:`laa_overview` section.
* **LAVA:** Linaro Automated Validation Architecture
* **LMS:** LAVA Managed Service. See th:ref:`lms` section. 
* **MIB:** Mechanical Interface Board. See the :ref:`laa_mib` section.
* **OTA:** Over-the-Air, typically associated with Over-the-Air software or
  firmware updates.  
* **PCB:** Printed Circuit Board
* **PARSEC:** `Platform AbstRaction for SECurity <https://parsec.community/>`_
* **PSA:** `Platform Security Architecture
  <https://www.arm.com/architecture/security-features/platform-security>`_
* **TPM:** Trusted Platform Module 




Linaro Remote Labs FAQs
***********************

.. list-table:: **Linaro Remote Labs FAQs**
   :header-rows: 1

   * - Questions
     - Answers
   * - question 1 tbd
     - answer to question 1 tbd



Feedback and support 
********************

To request support please contact Linaro at support@linaro.org.

Maintainer(s) 
*************

- Don Harbin <don.harbin@linaro.org>
