..
 # Copyright (c) 2022, Arm Limited.
 # Copyright (c) 2023, Linaro Ltd.
 #
 # SPDX-License-Identifier: MIT

Platform Enablement Overview 
############################ 

Platform Enablement Summary
===========================

When a platform is evaluated for inclusion in any
Remote Lab, there are several logical steps that need understood and need
stepped through. This
section provides an overview of this process and focuses on the portions of the
enablement process that are common across all Linaro Remote Labs.

.. _assessment_review:

Comprehensive Assessment Review
*******************************

The Comprehensive Assessment Review is a joint activity where Linaro
experts meet with the partner to evaluate what's needed to add
hardware to one of the :ref:`linaro_labs` instances.  Although each lab type
is similar, each lab also has unique characteristics that
may require additional details which are discussed in the section
specific to the lab instance being enabled. 

.. _laa_onsite_setup: 

Onsite Device Setup
*******************

This section assumes that the :ref:`assessment_review` has been completed and
the user has received their LAA along with the appropriate MIB and any
supporting equipment.

The steps to setup the LAA and DUT, including LAA unboxing, bring-up,
and initial configuration, is common across all of Linaro Labs. For
this initial DUT configuration activity, please follow the steps in the
:ref:`laa_interface_guide`.


Remote Lab Connection
*********************

After device setup is complete, the next step in platform enablement is
connecting the LAA over the cloud to your Linaro Remote Lab service. Connecting to a
remote lab is a lab-dependent activity and can differ depending upon the type
of Linaro Remote Lab the user is connected to. Please go to the appropriate lab
found in the :ref:`linaro_labs` section of this document. 

Lab Test Execution & Validation Service
***************************************

Once, your LAA is configured and it has been registered with Linaro Remote Lab,
then you're all set. At this stage you should be able to trigger tests and
they will run on your board.

Results Dashboard Access
************************

The final high level step is setting up the results dashboards to meet the goals
of participating in the Lab. Goals can range from a private instance used for
internal performance testing to sharing results with partners to provide
solution confidence, to sharing a fully public dashboard that carries the
benefits of marketing a compliant ready product solutions

