..
 # Copyright (c) 2022, Arm Limited.
 # Copyright (c) 2023, Linaro Ltd.
 #
 # SPDX-License-Identifier: MIT

Legal and Regulatory Compliance
###############################

FCC Mark and CE Mark Conformance Disclosure
*******************************************

TODO: FILL IN


Waste from Electrical and Electronic Equipment (WEEE)
*****************************************************

TODO: FILL IN ADDITIONAL DETAILS

The WEEE Directive(Directive 2012/19/EU) is a European Union legislation aimed
at reducing the environmental impact of waste electrical and electronic
equipment by promoting its collection, recycling, and responsible disposal.

Linaro is fully committed to complying with the WEEE Directive, ensuring
responsible disposal and recycling of electronic and electrical equipment to
protect the environment and promote sustainability.

Product Disposal Instructions
=============================

All devices should be returned to Linaro for proper product disposal. Please use
the following address:

Linaro Product Disposal

For more information, see :ref:`laa_contact`.

Collection and Recycling Information
====================================

TBD - Details on take back scheme and local collections services.  

To access our recycling services, please visit our website or contact out
customer service team for information on local collection points and take-back
programs designed to facilitate the environmentally responsible disposal of your
electrocis products.

Environmental and Health Information
====================================

TODO: A summary of compliance with national WEEE regulations, especially if
operating in multiple countries with varying requirements. 

Legal Compliance
================

TODO: 

WEEE Registration Details
=========================

TODO:  WEEE Registration number, companies this applies, 

For more information, see :ref:`laa_contact`.


Updates and Ammendments
=======================

TODO:Updates and Ammendments


.. _laa_contact:

Contact Information
===================

TODO: Address and Phone Here
