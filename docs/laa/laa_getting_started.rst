..
 # Copyright (c) 2022, Arm Limited.
 # Copyright (c) 2023, Linaro Ltd.
 #
 # SPDX-License-Identifier: MIT

.. _laa_interface_guide:

LAVA Automation Appliance Getting Started
#########################################
If you're reading this section, you've received your LAA and are preparing to
set it up.  Congratulations!  This section will walk you through these steps.

.. warning:: It is highly recommended that for all the steps in this section
   where the developer is handling or working on an LAA, that the user follows
   industry standard anti-static guidelines. This includes use of a grounded
   wriststrap as to place all the exposed hardware only on properly grounded
   anti-static mats.


.. _laa_unboxing: 

LAA Unboxing
************
When you receive your package you will be receiving several important items in
the box.  This includes the following:

- :ref:`laa_lsib`
- :ref:`laa_mib`
- LAA Configuration Reference Card. This card includes important information
  about you LAA such as the IP address needed to log into and configure your
  LAA.

.. warning:: Don't lose your Configuation Reference Card. It's recommended to
   take a photo of it and archive for future use. 

TODO: INSERT Picture of the Configuration Reference Card.

TODO: INSERT UNBOXING VIDEO HERE


Assembling the LAA
*********************
.. warning:: No equipment should be powered on while performing the setup in
   this section. That includes assuring that ethernet and USB cables. The last
   step of this section will be to apply power. 

If your setup required the inclusion of an MIB, mount the MIB on the included
LSIB using the 96-pin keyed connector. 

Mount your DUT to the MIB.

If required, directly connect the Ethernet connector directly from the LSIB to
the DUT. This direct connection may apply to the high speed USB connection.

Connect any additional custom cabling as determined by the result of the
guidance from Linaro during the :ref:`assessment_review`. 

The LAA is now ready to power on. Plug in the power adaptor and turn the power
on. See the figure below that shows the device has powered up correctly

TODO: ADD FIGURE OR VIDEO - SHARE WHAT LED MEANS THE LAA CAME UP WITH NO ERRORS.

TODO:  Add new section that defines LED, OLED, What each means

Connecting to the LAA
*********************
The LAA is headless, meaning that an external computer such as a laptop, a
Chromebook or any similar device must be connected. The external computer
requires two things, a phycial ethernet port (RJ45), and a browser supported OS
such as Linux, Windows, ChromeOS or other. 


.. note:: 
   If device is able to attain an IP address through DHCP, then a user will be
   able to connect to the LAA UI using the IP address shown on the LAA OLED. 

   If local network requires a static IP Address, then it's expected that a
   cross-over ethernet cable should be attached from the local computer to the
   DUT side RJ45 port on the LAA for direct connection.

.. note:: Once the LAA is attached to the local network it will attempt to
   connect to the update manager in the cloud and will go through an auto-update
   cycle if required

Connect you external computer to the LSIB using an Ethernet cable

TODO:  Crossover Ethernet OR would need an external switch

DUT Integration
*********************
After a DUT is connected to the LAA, there are still several steps required to
prepare the DUT to be "LAVA Ready."  This includes the following:

* Creation of a platform Device Dictionary
* Development and successful passing of a LAVA "health check"
* Device Definition upstreaming

The above are all standard steps in enabling any platform into Linaro
Automation Validation Architecture (LAVA) test automation framework and are
described in more detail in the following sections.


.. _laa_config:

Configuring the LAA
*******************


*CONFIRM* Many of the interfaces on the LAA are configurable. Once logged into
the LAA either through a USB port C defaulted for use  as a serial console, the user can now validate each interface to assure each are enabled and working properlay.

TODO: Add recommended process of verifying the intefaces

To enter the Configuration Wizard, enter the following:

```
$ laa_config
```

Configuration options are described below:

TODO: Add tips to verify each required interface

- Ethernet: Determine which port connects to the ONELab Cloud and which connect
  to the DUT 
  - Configure IP address for both Cloud and DUT connectivity as well.

- USB:  ADD CONFIG here



Device Dictionary Creation
**************************
In most cases, when an LAA is recieved the user will have already worked with
Linaro to assure the LAVA Device Dictionary has been enabled and functional for
the platform.

If a platform does not have a previously developed `Device Dictionary
<https://docs.lavasoftware.org/lava/glossary.html#term-device-dictionary>`_
the user will have the option of leveraging Linaro to help or to enable it
themselves.

Device Type Upstreaming
***********************
An important step in assuring that a platform continues to work when components
in Linaro Remote Labs is to contribute the device type enablement scripts are
`contributed upstream <https://docs.lavasoftware.org/lava/development-intro.html#contribute-upstream>` into the LAVA development repository.


