..
 # Copyright (c) 2022, Arm Limited.
 # Copyright (c) 2023, Linaro Ltd.
 #
 # SPDX-License-Identifier: MIT
 
Linaro Automation Appliance FAQs
################################

.. list-table:: **Linaro Automation Appliance LAA FAQs**
   :header-rows: 1

   * - Category
     - Questions
     - Answers
   * - Device Under Test
     - How do I purchase a Linaro Automation Appliance MIB?
     - Find this by going `here <https://www.linaro.org/>`
   * - 
     - Which MIB do I need?
     - This question is answer by going through the :ref:`assessment_review`
       with Linaro. 
   * - Power and Electrical
     - What voltage is required for my platform?
     - This is platform independent
   * - 
     - How is the LAA powered?
     - The LAA can be plugged into a  standard 120ac or 220ac outlet. The LAA can be ordered
       with US, UK, and European plugs.
   * - 
     - My LAA shuts down when my DUT is connected and powered on - what's going on?
     - Your DUT might have a problem where it's overdrawing current (3-5Amps)
       whereby the LAA shuts down in order to protect itself. You should try
       another DUT of the same type and see if that works.  
   * - Connecting to
       your LAA
     - How to I know the LAA IP Address?
     - When you received your LAA, there will be a LAA Getting Started Card
       included. This card will have the device IP address on it. It's a good
       idea to save this IP address for future use. See the :ref:`laa_unboxing`
       section for more information.


