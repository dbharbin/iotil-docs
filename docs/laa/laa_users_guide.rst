..
 # Copyright (c) 2022, Arm Limited.
 # Copyright (c) 2023, Linaro Ltd.
 #
 # SPDX-License-Identifier: MIT

.. _laa_users_guide:

Linaro Automation Appliance Users Guide
#############################################

Remote Debugging
****************

TODO:


Fleet Management
****************

TODO:

Assess Management
*****************

TODO:

Retrieving and Understanding Logs
*********************************

TODO:

DUT Remote Console
******************

TODO:

LAA Remote Console
*******************

TODO:


LAA Manual Recovery
*******************


TODO:


LAA CLI
*******

The LAA has a custom CLI for configurating and acessing LAA functionality. For a
comprehensive list of the LAA CLI commands 'click here <>'_ 

NOTE: LAA_CLI MAY BE INTERNAL ONLY...


