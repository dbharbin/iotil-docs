..
 # Copyright (c) 2022, Arm Limited.
 # Copyright (c) 2023, Linaro Ltd.
 #
 # SPDX-License-Identifier: MIT


Linaro Automation Appliance Reference Content
#############################################

.. _laa_board_design_reference:

Platform Design Reference Guide
*******************************

This section is a reference section for a a hardware/platform designer to
reference prior to building a platform that is intended to be placed into any
type of Test Automation Framework (TAF) or CI infrastructure.


LSIB Pin Header Definition
**************************

TODO: Place Pin Header here


LSIB Schematics/Layout Reference
********************************


TODO: Place high-level schematics here for LSIB - Perhaps just connectors?


LSIB Component Description
**************************

TODO: Add LAA component details here



