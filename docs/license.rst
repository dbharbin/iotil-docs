#######
License
#######

TBD

****************
SPDX Identifiers
****************

Individual files contain the following tag instead of the full license text.

TBD

This enables machine processing of license information based on the SPDX
License Identifiers that are here available: http://spdx.org/licenses/
