..
 # Copyright (c) 2022, Arm Limited.
 # Copyright (c) 2023, Linaro Ltd.
 #
 # SPDX-License-Identifier: MIT

.. _onelab_overview:

ONELab Overview
################
Welcome to Linaro's ONELab specific documentation. ONELab is a Testing Service
for Arm platforms that proves standards compliance and demonstrates
compatibility of platforms and payloads in various environments. Multiple
variants of ONELab Integration Labs are planned to exist over time with a common
goal, that being the provision of a cloud based lab for standards compliance.
ONELab Integration Lab instances are designed to demonstrate compliance in
various ecosystems. For example the :ref:`onelab_iot_entry` has been designed
specifically to demonstrate SystemReady IR and PSA compliance for participating
platforms. In addition, ONELab Integration Labs may validate cloud native
applications and tooling solutions that also need to demonstrate compatibility
and operations compliance to the standards being validated.  Applications that
leverage the middleware can also be validated across platforms residing in
ONELab.

.. figure:: ../images/ONELab_UI_v02.png 
   :alt: descriptive text
   :align: center
   
   Snapshot of the Linaro ONELab IoT Explore Panel

.. _onelab_mission:

Linaro ONELab Mission
=====================
Our mission is to ensure the highest standards of reliability and
interoperability for Arm-based platforms and software by rigorously validating
their compliance with Arm SystemReady standards. We aim to empower manufacturers
and developers by certifying that their products meet established benchmarks for
security and compatibility. By doing so, we provide OEMs and ODMs with the
confidence to choose validated solutions, fostering trust and driving innovation
across the Arm ecosystem 

.. _onelab_functional_summary:

ONELab Funtional Summary
========================

Benefits of this service include:

* Self-service capabilities
* Low cost of onboarding
* User-friendly focus
* Continuous validation for updates to operating systems, test suites, and
  firmware
* Remote lab support 
* Independent evaluation of 3rd party compliance test suites
* Enhancing customer confidence

To review specific benefits provided by a particular ONELab Integration Lab
instance, please see the section associated with the Lab of interest.

* :ref:`onelab_iot_entry`

Once a platform, operating system, or application is integrated into ONELab, it
automatically generates test results in response to specific events such as
firmware updates, OS updates, and updates to compliance test suites. It also
offers a dashboard that displays the platform's status against these test
suites. Participating vendors can choose how they share this dashboard—keeping
it private, making it public, or sharing it with selected customers.

Additional features and benefits provided by ONELab participation
include:

* Upload Results to ONELab’s Live/Searchable Conformance Results Dashboard
* Displays up-to-date available qualified platforms and OS’s
* Provides confidence in preparation of devices targeted for compliance is such
  programs as the `EU MDR <https://eumdr.com/>`_ and `USA FDA 510K
  <https://www.fda.gov/medical-devices/how-study-and-market-your-device/estar-program>`_

Continuous testing loops triggered by test environment changes to the above
maintain compliance verification for firmware updates and prevention of
compliance regressions. Environmental changes may include firmware updates, OS
or Payload updates, or even test suite updates.  This gives SoC vendors, OEMs,
and ODMs alike confidence that their software-hardware platform solutions are
prepared to meet System Ready IR certification.

ONELab is built upon Linaro Managed Services (see :ref:`lms` for more
information).  However, when it comes to the :ref:`linaro_labs` Integration
Labs, Linaro has already taken care of all the lab infrastructure details, so
users of the lab don't need to be be distracted by this level of detail. The focus
can be on enabling your platform on the Linaro Automation Appliance, plugging
your platform into ONELab infrastructure, and begin gathering the results!

