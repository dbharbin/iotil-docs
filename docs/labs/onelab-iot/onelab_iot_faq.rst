..
 # Copyright (c) 2022, Arm Limited.
 # Copyright (c) 2023, Linaro Ltd.
 #
 # SPDX-License-Identifier: MIT

ONELab IoT FAQs
################

This section provides additional information in regards to Linaro's IoT
Integration lab in the form of frequestly asked questions

ONELab IoT Frequently Asked Questions:
======================================

.. list-table:: **Linaro ONELab-IoT FAQs**
   :header-rows: 1

   * - Category 1
     - Question
     - Answers
   * - 
     - Question
     - Anwwer
   * - 
     - Question
     - Anwwer
   * - LAVA CI
     - Where can I see the LAVA Jobs running in ONELab IoT?
     - They can be found here: https://iotil.validation.linaro.org/scheduler/alljobs 
   * - 
     - Where is the list of available Device Types?
     - The list of Device Types are here: https://iotil.validation.linaro.org/scheduler/device_types 



