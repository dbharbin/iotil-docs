..
 # Copyright (c) 2023, Linaro Ltd.
 #
 # SPDX-License-Identifier: MIT

.. _onelab_ui:

ONELab IoT User Interface
#########################

This section provides example usages of the ONELab IoT UI

ONELab IoT Logging in
*********************
When initially accessing ONELab IoT, the Explore Boards page is displayed.
If not logged in, the user can only see the Explore Board.
Log in is managed by organization (or company).
Once logged in, a user has access to the organization Dashboard and Appliances
pages through the hamburger button on the left.
The user can also now schedule Conformance Runs and access the Appliances
Dropdown

ONELab IoT Search
*****************
The ONELab Dashboard provides a search bar. It can search based on Board Name
or Firmware Name. The search isn't case sensitive. When you select the items
found, it will display them in the results space when clicked. Use the "Clear
Filters" selection to return the Dashboard to show all Conformance Runs that
your login provides you access to.

The example below describes how to use the search function in the ONELab IoT
Dashboard:

.. video:: ../../_static/videos/ONELabSearchBar.mp4
   :width: 640
   :height: 480
   :poster: ../../_static/images/ONELabSearchBar.png
   :class: tight-spacing

|

ONELab IoT Accessing the Dashboard and Appliances Pages
********************************************************
When a user initially navigates to the ONELab website, they will land on the
Explore page. From there the user may log in. Once logged in, a user has access
to the Dashboard and Appliances pages for the organization of which they are a
member. Access these by clicking on the hamburger button on the upper left
portion of the screen.  

The Dashboard provides a summary of :ref:`Assessments <assessment>`, Appliances, and :ref:`Release Streams <release_stream>`

TODO: What's an Assessment?

CHECK: Release stream versions, Total Versions - can't see them.

The Appliances Page show all Appliances available within this organization along
with there status (offline or online). The user can narrow down the Appliances
displayed by selecting a specific platform from the Device Type Dropdown.
Selecting the Kebab (three dots) Icon to the right of the Appliance allows the
user to Configure an Appliance


ONELab IoT Configuring an Appliance
***********************************
An Appliance can be configured from the Kebab Icon on the Appliance Page

CHECK: What am I Configuring? I can change a device Type in re-configuring?

ONELab IoT Scheduling a Conformance Run
***************************************

To Schedule a :ref:`conformance run, <conformance_run>` click the Conformance Run button from either the
Dashboard or the Explore Board, select an existing Release Stream or Click the
`Add new release stream` button to add a new one.  Note that this defaults to
Public visibility, thus to make private, be sure the toggle to Private

CHECK: Suggest Default is Private! That way harder to accidentally post a private
build to the public

Add a Version String text and Version Description. This is a text stream and can
contain whatever text the user chooses to help identify the Conformance Run.
Then click Schedule Conformance Run

CHECK: What is a release stream?

When a Target Device that matches the one in your Release Stream is
available, testing will be begin.

CHECK: How notified when Complete?

Diagram Test
************

.. mermaid::

   sequenceDiagram
     actor Alice
     participant Bob
     participant Don
     Alice->>Bob: Hi Bob
     Bob->>Alice: Hi Alice
     Alice->>Don: next test


