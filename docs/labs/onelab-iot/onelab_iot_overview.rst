..
 # Copyright (c) 2022, Arm Limited.
 # Copyright (c) 2023, Linaro Ltd.
 #
 # SPDX-License-Identifier: MIT

.. _onelab_iot_overview:

ONELab IoT Overview
###################
ONELab IoT is a testing service for Arm platforms that verifies standards
compliance for SystemReady IR and PSA platforms. It ensures compatibility with
SystemReady IR operating systems, primarily Linux, and confirms the correct
functioning of cloud-native tools and environments designed for IoT edge
devices. ONELab IoT also tests the installation and operation of operating
systems intended for edge devices on SystemReady IR compliant platforms.
Additionally, since operating systems, firmware, and cloud-native applications
are frequently validated online for correct installation, payload validation is
a crucial component of the ONELab IoT test suites.

These services add value to participating platforms, cloud-native services, and
operating system providers by demonstrating their interoperability with the
SystemReady IR compliance standards. This assurance boosts customer confidence
for OEMs and ODMs when selecting solutions for their end products.

.. image:: ../../images/ONELab_UI_v02.png 

Linaro ONELab IoT Mission
=========================
The IoT Lab extends the overall :ref:`onelab_mission` with three primary test
suites:

* SystemReady IR `Architecture Compliance Suite(ACS) tests <https://github.com/ARM-software/arm-systemready/tree/main/IR>`_
* Arm Platform Security Architecture(PSA): Test Suite

  * Verifies Crypto, Storage and Attestation compliance of the device
  * The parent repo for this test suite is located `here <https://github.com/ARM-software/psa-arch-tests>`__
  * The API test folder is `here
    <https://github.com/ARM-software/psa-arch-tests/tree/main/api-tests>`__
  * Under that folder a user can find the dev_apis folder with Crypto API tests
    `here
    <https://github.com/ARM-software/psa-arch-tests/tree/main/api-tests/dev_apis>`__
  * And the Crypto API specs are located `here
    <https://arm-software.github.io/psa-api/crypto/>`__

* Test for Parsec compliance leveraging the `Parsec Test Suites
  <https://github.com/parallaxsecond/parsec>`_

ONELab IoT Functional Summary
=============================

In addition to the standard :ref:`onelab_functional_summary`, several additional
benefits of the ONELab IoT service include:

* Continuous validation as OS’s, Test Suites, and F/W are updated against
  release versions
* Dashboard results providing latest status of compliance tests of participating
  platforms, OS's and Payloads to assure compliance to the following:

  * SystemReady IR (IoT Ready)
  * PSA API 
  * PARSEC Test Suite

Examples of functionality validated to assure SystemReady IR compliance include:

* FOTA, Shared Storage, and Capsule update requirements
* UEFI update, UEFI Secure Boot / TPM extensions
* Device Tree Conformance
* Booting of a variety of SystemReady IR enabled OSs (Debian, Yocto, Fedora)
* Running of the latest ACS (ARM Compliance Suite) 
* Support for specialized firmware variants targeted for compliance validation 

  * example: variants for regional wifi requirements 


ONELab IoT Enablement
=====================

The initial steps to enabling a platform are common across ONELab Labs. These
are outlined in the :ref:`onelab_getting_started` section.  This section
identifies exceptions / steps that are unique to the common getting started
section.

.. note:: Please read the following section and become familiar with the
   exceptions to the special cases for getting started with ONELab IoT prior to
   following the steps in the general :ref:`onelab_getting_started` section.


.. _onelab_iot_prereqs:

ONELab IoT Specific Prerequisite Requirements
=============================================

For integration into ONELab IoT Integration Lab, the following prerequisite
requirements must be met.

Platform
--------

#. The board is capable of running the Arm SystemReady Architecture
   Compliance Suite (ACS) test suite for SystemReady IR.
   
   Note this doesn't mean it's been certified, just that it's able to run the
   tests. Contact `Linaro Developer Services <https://www.linaro.org/services/>`_
   if support needed in preparing a platform to pass SystemReady IR.

#. Linaro strongly recommends that the board passes all applicable ACS tests
   since that shows the board has the capability to run other ONELab IoT tests.

   If there are any known failures they should be discussed as part of the
   Comprehensive Assessmemt Review.

#. The platform may also be `PSA Certified <https://www.psacertified.org/>`_ but
   this is not a hard requirement.  As a reminder, the high level requirements
   for this include the following:

   * Support for Secure Boot
   * Support for H/W Root of Trust functions to support secure operations such
     as key generation & storage, identity, and authentication
   * Secure storage of sensitive data such as keys & credentials
   * Secure and reliable software update
   * Threat protection as outlined int eh PSA Threat Model and Security Analysis
     document (TMSA).
   * Cryptographic function support (encryption/decryption, hashing & digital
     signatures
   * Authentication/authorization access control
   * Session protection for secure inter-device communication
   * Audit & Logging of security relevant events
   * Detection of analogous behaviors
   * Other general requirements such as Physical Security (anti-tamper), attack
     recovery, and a secure lifecycle management solution are also included

   As noted above, `Linaro Developer Services
   <https://www.linaro.org/services/>`_ can provide support in meeting the above
   if help needed.


Payload
-------

The payload shall be provided in a binary SystemReady IR compliant image. The
image can be accepted if **any** of the following criteria apply:

1) An application that leverages the PARSEC API
2) Middleware that provides support for PARSEC
3) An OS that supports installation from SystemReady IR conformant firmware

Once payloads comply with one of the above, ONELab IoT automations will trigger
new test runs whenever relevant updates occur in the infrastructure and the
Dashboards will be updated with the latest results related to the payload under
test.

