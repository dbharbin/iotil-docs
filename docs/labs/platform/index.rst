..
 # Copyright (c) 2023, Linaro Ltd
 #
 # SPDX-License-Identifier: MIT

.. _onelab_getting_started:

ONELab Getting Started
======================

.. note::
   Note that ONELab is a paid service from Linaro requiring a user to have a
   registered account. Most of the steps in this Getting Started section require
   that this has been attained. To learn more about becoming a registered user
   of ONELab, please visit the :ref:`onelab_account_reg` section of this
   document. 



.. toctree::
   :maxdepth: 3

   onelab_platform_enablement

