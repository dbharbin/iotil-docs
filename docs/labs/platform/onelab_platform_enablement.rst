..
 # Copyright (c) 2022, Arm Limited.
 # Copyright (c) 2023, Linaro Ltd.
 #
 # SPDX-License-Identifier: MIT

ONELab Account Registration
###########################

In order to have a platform or payload enabled in ONELab, a user must have set
up an account with Linaro. Once your account is set up, you will be provided a
login at the level that supports the account level set up.

TODO: Add content here or in a common section on Account types, registering,
contract, etc.

ONELab Platform Enablement Process
##################################

To bring up a platform in ONELab, there are several steps required. The figure
below breaks this process into 5 high-level sequential steps.

.. image:: ../../images/platform_enablementv0.2.png

The remainder of this section will walk through the details of each of these
steps.


.. note:: This section provides the startup steps common across all ONELab
   instances. It's recommended to become familiar with the Enablement chapter in
   the Overview section of the specific lab being enabled prior to continuing
   with this section.

Prerequisite Requirements
*************************

For integration into ONELab, the following platform requirements must be met.

#. The platform should implement the :ref:`laa_minimum_featureset`
#. EFI HTTP boot via wired ethernet must be implemented, and boot target must be configurable via the
   serial interface.
#. There is a LAVA device dictionary to map LAA to DUT power, ethernet, serial and
   reset.

As always, `Linaro Developer Services <https://www.linaro.org/services/>`_ can
provide support in meeting the above should help be needed.

.. note:: In addition to these Generic requirements, each ONELab lab instance
   will have lab-specific requirements associated with it. To see these, please
   go to the following sections:

   - :ref:`onelab_iot_prereqs`

.. _onelab_car:

ONELab Comprehensive Assessment Review
**************************************

To get started in the process of integrating a platform into ONELab, the
platform is analyzed to determine its readiness to be added to ONELab.  The
output of this step will included recommended next steps for ONELab Readiness.
To a large degree, this is a "Desk Review".  To complete this step, a company
will work closely with Linaro experts to go through a platform assessment.

Once the this process has been completed with Linaro, an LAA
with the recommended :ref:`laa_mib` is shipped to the location where the LAA
will be enabled. This may be a partner facility or a Linaro location if it's
determined that Linaro support is the best option for expedient enablement. The
DUT, MIB, and LSIB will be assembled and the assembled system will be ready to
go through the :ref:`onelab_onsite_setup`.

The review will be undertaken as follows:

#. Schedule the joint Linaro-Partner Assessment Review

   This will typically be scheduled a month out to give the partner time to perform the
   interim following steps.

#. Partner: Fill out the ONELab assessment form and return to Linaro.

   The assessment is provided by Linaro to the partner and includes a list of
   questions as well as the ability to download both Schematics and Board layout
   for Linaro experts to review.

   The assessment form is tailored for the appropriate ONELab Integration Lab instance
   will be shared with you prior to the :ref:`onelab_car` meeting. The completion of this
   form will aid in the efficiency of the Assessment Review and assure that both
   teams are aligned on the plans to enable the platform. The focus of the
   Assessment Review is to understand how prepared the platform is for
   automation.

   .. note:: See the :ref:`laa_overview` section of this document that provides an
      overview of considerations a vendor should consider when developing new
      platforms that are anticipated to be placed into a Test Automation Framework
      such as Linaro Remote Labs.

#. Linaro/Partner: Conduct the scheduled Assessment review meeting and Review the
   results of the checklist and any relevant documentation to determine next steps.
   This may include any of the following:

   * Is the board suitable for automation? If so, provide the partner with recommended next steps.
     * Are physical modifications required to automate it?
     * Are there any software features that need to be implemented and/or tested?
   * Is a MIB required, if so which one?
     * If a custom MIB needed, then create a plan to have it created.
   * Does the platform need of custom LAVA Device Dictionary development? If so
     create the plan/strategy to complete this.

.. raw:: latex

   \newpage


.. _onelab_onsite_setup:

ONELab Onsite Setup
*******************

Referring back to the diagram, once the :ref:`onelab_car` has been completed,
the required equipment, including an LSIB and MIB are shipped to the planned
remote lab location, which is typically a customer facility.

.. note:: For this step, it is may be necessary for the Linaro Engineering team
   to undertake some pre-work to support the initial bringup. Pre-work will be
   identified during the Assessment Review and, once completed, the working
   setup sent to the customer premises for completion of the bringup.

Once received, the setup is now ready to be assembled and configured. To do
this, please go through the configuration steps outlined in the
:ref:`laa_onsite_setup` section.

The ONELab Onsite Setup may take the form of two steps. Initially, it may be
necessary for the Linaro expert developers to perform some of the initial
enablement steps prior to shipping an LAA to the ONELab customer.
Once shipped, the user will follow the :ref:`laa_interface_guide` section to
complete the bringup of the LAA.

.. warning:: It is necessary for the LAA to establish outbound ethernet
   connections to register with the cloud service and poll for its next task.
   This step may require the partner company to work with their IT department
   to enable suitable network access for the LAA.

Connect to the ONELab Cloud Service
***********************************

Once the LAA is setup with the DUT installed the LAA requires connection to the
ONELab Cloud Server in order to start running test jobs on the board.

It is necessary for the LAA to establish outbound ethernet connections to
register with the cloud service and poll for its next task.  This step may
require the partner company to work with their IT department to allow the LAA
interoperate with the corporate firewall.

.. Note::

   The ONELab Cloud Service will never initiate access into a corporate
   infrastructure. The LAA always initiates outbound connections to the ONELab
   Cloud Server from within the corporate facility.

.. _onelab_ac_config:

New Platform/Device Type Onboarding
************************************

TODO: Describe how to get a new platform added to the selectable target list.

Enrolling an LAA into a ONELab instance
***************************************

After completing
TODO: Fill In

Setting up Dashboard Access Control
***********************************

TODO: Describe how to construct a dashboard, manage user/group access,
set up visibility restrictions and configure notifications.

Uploading Firmware
*******************

After successfully connecting to the Cloud Server, the user can now schedule
a conformance run and upload the firmware binaries to test for compliance.
When scheduling a test run for the first time you will need to add a new
:ref:`Release Stream <release_stream>`.  These actions will automatically kick off the CI and
allow the results to flow into the appropriate dashboards.

.. note:: Typically, when you schedule a conformance run, you will re-use an
   existing Release Stream. It is only necessary to add new Release Streams
   when a new family of firmware is tested for the first time.

Understanding the CI Automations
********************************

TODO: Describe when tests are run (and what triggers them).

Reading the results
*******************

Once tests have kicked off, the user is now ready to evaluate results.

ONELab provides a dashboard that displays the latest results as shown in the
following figure.

.. image:: ../../images/ONELab_UI_v01.png

In the figure, there are several distinctions to be considered.

- Each row represents a platform that is currently under test in ONELab. The
  default view is to see all platforms whose results have been configured for
  public access.  See the :ref:`onelab_ac_config` section on how to set this up for
  your platforms.
  - To view your private dashboard, the user must login

.. _onelab_account_reg:

ONELab Payload Enablement Process
##################################

TODO: Describe how to enable new tests (if applicable).

Generic Payload Requirements
****************************

Any payloads shall be provided in a binary image.  Once an image complies with
the above, ONELab automation will trigger new test runs whenever relevant
updates occur in the infrastructure and the Dashboards will be updated with the
latest results related to the payload under test.

Each payload is required to provide a Python interpreter that can parse the
results from the payload application and produce LAVA test results.

ONELab Distribution Enablement Process
######################################

TODO: Describe how to add a new distribution to the test matrix.
