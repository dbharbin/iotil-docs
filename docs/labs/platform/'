..
 # Copyright (c) 2022, Arm Limited.
 # Copyright (c) 2023, Linaro Ltd.
 #
 # SPDX-License-Identifier: MIT

ONELab Platform Enablement Process
##################################

To bring up a platform in ONELab, there are several steps required. The figure
below breaks this process into 5 high-level sequential steps.

.. image:: ../../../images/platform_enablementv0.2.png

The remainder of this section will walk through the details of each of these
steps.

.. _onelab_car:
 
ONELab Comprehensive Assessment Review 
**************************************

To get started in the process of integrating a platform into ONELab, the
platform must be analyzed to determine its readiness. To complete this process,
a company will work closely with Linaro experts to go through a platform
assessment. The details of this process are provided in the
:ref:`assessment_review` section of this document.  

Once the this process has been completed with Linaro, an LAA with the
recommended :ref:`laa_mib` is shipped to the location where the LAA will be
enabled. The DUT, MIB, and LSIB will be assembled and the assembled system will
be ready to go through the :ref:`laa_onsite_setup` will be completed.

Prerequisite Requirements
=========================

For integration into ONELab, the following requirements much be met.

Platform
--------

1. The board is passing a SystemReady IR test suite; it may still need
   certification.  Contact `Linaro Developer Services
   <https://www.linaro.org/services/>`_ if support needed in preparing a
   platform to pass SystemReady IR.
2. It is possible to arrange for the test device to establish an outbound-only
   secure ethernet connection to poll the cloud service for its next task.
   **OPEN** -
   need a page to expand on this Ryan
3. `PSA Certified <https://www.psacertified.org/>`_

   * Support efi HTTP boot  - once again, contact `Linaro Developer Services
     <https://www.linaro.org/services/>`_ if help needed.
4. There is a device dictionary to map LAA to DUT power, ethernet, serial and
   reset. 

Payload 
-------

The payload shall be provided in a binary SystemReady IR compliant image. The
image can be accepted if **any** of the following criteria apply:

1) An application that leverages the PARSEC API
2) Middlewhere that provides support for PARSEC
3) An OS that supports installation from SystemReady IR conformant firmware 

Once payloads comply with one of the above, ONELab automations will trigger
new test runs whenever relevant updates occur in the infrastructure and the
Dashboards will be updated with the latest results related to the payload under
test.

Each payload is required to provide a Python interpreter that can parse the
results from the payload application and produce LAVA test results.

ONELab Onsite Setup
*******************

Referring back to the diagram, once the :ref:`onelab_car` has been completed,
the required setup including an LAA and MIB is shipped to the planned remote
labe location, typically at a customer facility. The setup is now ready to be
assembled and to go through the configuration steps outlined in the
:ref:`laa_onsite_setup` section.

Connect to the ONELab Cloud Service
***********************************

Once the LAA is setup with the DUT installed and is passing local testing per
the previous section, the LAA requires connect to the ONELab Cloud Server to
begin ONELab CI testing. This will often require the ability for the LAA to be
given permission from a corproate IT group to go through a corporate firewall.  

.. Note:: 

   The ONELab Cloud Service will never initiate access into a corporate
   infrastructure. The LAA always initiates the connection to the ONELab Cloud
   Server from within the corporate facility.

TALK TO SOMEONE ON THIS!!!

.. _onelab_ac_config:

Setting up Dashboard Access Ccontrol
**********************************

Set up visibility restrictions

Provide user/group access

Uploading Firmwware
****************

After successfully connecting to the Cloud Server, the user may now set up test configurations and upload current verison of Firmware that is desired to be tested for compliance.  These actions willl automatically kick off the CI to verify that the selected tests pass.


Understanding the CI Automations
********************************

When tests are run / triggers
Setting up notifications

Reading the results
*******************

Once tests have kicked off, the user is now ready to evaluate results. 

ONELab provides a dashboard that displays the latest results as shown in the
following figure.

.. image:: ../../../images/ONELab_UI_v01.png

In the figure, there are several distinctions to be considered.

- Each row represents a platform that is currently under test in ONELab. The
  default view is to see all platforms whose results have been configured for
  public access.  See the :ref:`onelab_ac_config` section on how to set this up for
  your platforms. 
  - To view your private dashboard, the user must login


