..
 # Copyright (c) 2023, Linaro Ltd.
 #
 # SPDX-License-Identifier: MIT

############
Contributing
############
We welcome contributions from anyone willing to
submit patches that conform to the licensing rules. 

Contribution Guidelines
***********************
The way to contribute is pretty much the same as what is usually done in open
source projects. All patches are integrated via GitLab Merge Requests.

Documentation Contribution Guidelines
*************************************
For guidelines to contribute to this document, please see the README in the `gitlab repository for this
document <https://gitlab.com/dbharbin/iotil-docs/-/tree/main>`_.

