..
 # Copyright (c) 2023, Linaro Ltd.
 #
 # SPDX-License-Identifier: MIT

.. _lms:

LAVA Managed Services 
#######################

.. toctree::
   :maxdepth: 1
   :caption: LMS Overview

   lmsoverview


.. toctree::
   :maxdepth: 1
   :caption: LMS Setup  

   lms_setup


.. toctree::
   :maxdepth: 1
   :caption: LMS API

   lms_api


.. toctree::
   :maxdepth: 1
   :caption: LMS FAQ

   lms_faq


