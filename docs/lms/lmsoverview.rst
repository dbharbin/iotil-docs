..
 # Copyright (c) 2023, Linaro Ltd
 #
 # SPDX-License-Identifier: MIT

.. _lms_overview:

LAVA Managed Services Overview
##############################

Test Automation Frameworks(TAF) can be quite complicated. They're composed of
many components including test harnesses, test case repos, data management,
configuration management, reporting and logging solutions, and notification
systems.  These must be housed on local servers to round out the solution on
what ends up being housed in multiple lab "racks." Then starts the real work
where a team of developers must maintain the systems, assuring they're upgraded
with the latest softwaere packages, OS's and security patches. Add to the the
support equipment needed for embedded TAF deployment that require pdu's, usb
hubs, relay solutions, swithes, and the list goes on, each of which provide
points of failure and lots of maintenance. And while a team of developpers is
maintaining this infrasture, what they aren't doing is advancing a company's
product!  

Enter Linaro's novel cloud-based TAF. At the center of Linaro's cloud-based
framework is LAVA Managed Services(LMS). LMS is a cloud-based solution to
traditional Test Automation Frameworks. Instead of the traditional TAF that is
built inside a physical lab with racks of servers and equipment to support the
required validation and test infrastructure, LMS has move this problem to the
cloud, and in addition to that, has removed many of the problems faced by
traditional TAFs permanantly. On top of that, LMS removes many of the barriers
that existed in traditional physical labs. LMS allows a user to deploy multiple
labs just as one might deploy a cloud-based compute resource. No longer is a lab
restrained by physical space and racks. No longer is a company spending time
maintaining these dedicated resources. The space and time are freed up to be
spend instead on product differentiation!  

This section will provide a detailed overview of how LMS accomplished this. It
will go over the LMS architecture that makes all this possible. A high-level
featureset provided by LMS includes the following:

- Orchestration of Labs: LMS automates the deployment, scaling, and management
  of containerized Labs, ensuring that they run efficiently and reliably across
  multiple Lab instances.
- Cross Lab DUT sharing: Supports sharing of DUT’s across multiple Lab instances
  providing increased efficiency of these devices.
- Load Balancing: LMS can be configured to automatically distribute test load
  traffic across Labs to ensure that the Labs stay accessible.  
- Automated Rollouts and Rollbacks: LMS supports automated Lab updates and
  rollbacks, enabling continuous/hands-free integration and delivery with
  minimal downtime and risk.
- Self-Healing: LMS monitors Lab containers and automatically replaces or
  restarts containers that fail, do not respond, or do not meet the user-defined
  health criteria.
- Security: LMS can deploy/manage Lab instances creating separate/secure
  integration labs. Additional end-user security provide confidence that test
  data only shared to those with access credentials
- Failsafe: LMS also automates backup and retention of the Labs providing
  high assurance against disaster scenarios
- Intuitive Dashboards / UI: Depending upon the particular lab instance and
  types of tests being executed, dashboards may be customized to show and
  compare results between various platforms or with previous versions of tests.
  Dashboards can adjust access control from public to private and focus on
  various purposes from demonstrating compliance test results to showing
  performance metrics


.. _managed_servers:

Managed LAVA Servers
====================


