#!/bin/sh
# Calculates a single build identifier hash based on repo and git tree
# status. Uncommitted changes will only impact the hash value and will not
# be highlighted. TRS bitbake distro config will use the same hash calculation.
set -euxo pipefail
printf "%.10s\n" \
	$( repo forall -c \
	     'printf "%s " $REPO_PATH && git describe --tags --always --dirty' | \
	   sha256sum | awk '{print $1}' )
